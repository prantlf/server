const test = require('ava')
const fetch = require('node-fetch')
const {Agent} = require('https')
const {connect} = require('http2')
const {createGunzip} = require('zlib')
const {StringDecoder} = require('string_decoder')
const concat = require('concat-stream')
const {startServer} = require('../helpers/startServer')
const tls = require('tls')

tls.DEFAULT_ECDH_CURVE = 'P-384:P-256'

const args = ['start'] // , '--config', 'http2server.config.js']

let node
test.before(async (t) => { node = await startServer({args}) })
test.after.always((t) => node.kill())

test('HTTP/1 with link preload over HTTPS', async (t) => {
  const url = 'https://localhost:8443/'
  const options = {agent: new Agent({rejectUnauthorized: false})}
  const response = await fetch(url, options)

  t.is(response.status, 200)
  t.is(response.ok, true)
  t.is(
    response.headers.get('link'),
    '</foo.%F0%9F%92%A9.js>; rel=preload; as=script, ' +
    '</stuff.txt>; rel=preload; as=fetch'
  )
  const body = await response.text()
  t.truthy(body.match(/Yay!/))
})

test('HTTP to HTTPS redirect', async (t) => {
  const url = 'http://localhost:8080/foo'
  const options = {redirect: 'manual'}
  const response = await fetch(url, options)

  t.is(response.status, 308)
  t.is(response.statusText, 'Permanent Redirect')
  t.is(response.headers.get('location'), 'https://localhost:8443/foo')
})

test.cb('HTTP/2 GET with Server Push', (t) => {
  const expected = {
    pushRequests: new Set([
      {
        ':path': '/foo.%F0%9F%92%A9.js',
        ':authority': 'localhost:8443',
        ':scheme': 'https',
        ':method': 'GET'
      },
      {
        ':path': '/stuff.txt',
        ':authority': 'localhost:8443',
        ':scheme': 'https',
        ':method': 'GET'
      }
    ]),
    pushResponses: new Set([
      {
        ':status': 200,
        'content-type': 'application/javascript; charset=UTF-8',
        'cache-control': 'public, max-age=31536000, immutable',
        'content-length': '70'
      },
      {
        ':status': 200,
        'content-type': 'text/plain; charset=UTF-8',
        'cache-control': 'public, must-revalidate',
        'content-length': '10'
      }
    ]),
    response: {
      ':status': 200,
      'content-type': 'text/html; charset=UTF-8',
      'cache-control': 'public, must-revalidate',
      'content-length': '111'
    }
  }

  const session = connect(
    'https://localhost:8443',
    {rejectUnauthorized: false}
  )

  const request = session.request({':path': '/'})

  session.on('stream', (stream, headers, flags) => {
    stream.on('push', (headers, flags) => {
      t.is(headers.server, undefined)
      const expectedPushResponse = Array.from(expected.pushResponses)
        .find((pushResponse) => {
          return Object.keys(pushResponse).every((header) => {
            return pushResponse[header] === headers[header]
          })
        })
      expected.pushResponses.delete(expectedPushResponse)
    })
    for (const pushRequest of expected.pushRequests) {
      if (pushRequest[':path'] === headers[':path']) {
        t.deepEqual(headers, pushRequest)
        expected.pushRequests.delete(pushRequest)
        return
      }
    }
    t.fail()
  })

  request.on('response', (headers) => {
    for (const name of Object.keys(expected.response)) {
      t.is(headers[name], expected.response[name])
    }
    t.regex(headers.server, /node/)
    request.resume()
  })

  request.on('end', () => {
    t.is(expected.pushRequests.size, 0)
    t.is(expected.pushResponses.size, 0)
    session.destroy()
    t.end()
  })

  session.on('socketError', t.end)
  session.on('error', t.end)
  request.on('error', t.end)
})

test.cb('HTTP/2 GET with GZip', (t) => {
  const session = connect(
    'https://localhost:8443',
    {rejectUnauthorized: false}
  )

  const request = session.request({
    ':path': '/stuff.txt',
    'accept-encoding': 'gzip'
  })

  request.on('response', (headers) => {
    t.is(headers[':status'], 200)
    t.is(headers['content-encoding'], 'gzip')

    request.pipe(createGunzip()).pipe(concat((data) => {
      const expectedBody = 'Blablabla\n'
      const body = new StringDecoder().end(data)
      t.is(body, expectedBody)
      t.end()
    }))
  })

  session.on('socketError', t.end)
  session.on('error', t.end)
  request.on('error', t.end)
})

test.cb('HTTP/2 HEAD', (t) => {
  const session = connect(
    'https://localhost:8443',
    {rejectUnauthorized: false}
  )

  const request = session.request({
    ':path': '/stuff.txt',
    ':method': 'HEAD',
    'accept-encoding': 'identity'
  })

  request.on('response', (headers) => {
    t.is(headers[':status'], 200)
    t.end()
  })

  session.on('socketError', t.end)
  session.on('error', t.end)
  request.on('error', t.end)
})

test.cb('HTTP/2 HEAD with GZip', (t) => {
  const session = connect(
    'https://localhost:8443',
    {rejectUnauthorized: false}
  )

  const request = session.request({
    ':path': '/stuff.txt',
    ':method': 'HEAD',
    'accept-encoding': 'gzip'
  })

  request.on('response', (headers) => {
    t.is(headers[':status'], 200)
    t.is(headers['content-encoding'], 'gzip')
    t.end()
  })

  session.on('socketError', t.end)
  session.on('error', t.end)
  request.on('error', t.end)
})

test.cb('HTTP/2 other methods are rejected', (t) => {
  const session = connect(
    'https://localhost:8443',
    {rejectUnauthorized: false}
  )

  const methods = ['POST', 'PUT', 'DELETE', 'PATCH']

  let counter = methods.length
  function onRequestEnd () {
    if (--counter === 0) {
      t.end()
    }
  }

  for (const method of methods) {
    const request = session.request({
      ':path': '/stuff.txt',
      ':method': method
    })

    request.once('response', (headers) => {
      t.is(headers[':status'], 405)

      request.pipe(concat((data) => {
        const expectedBody = 'Method Not Allowed'
        const body = new StringDecoder().end(data)
        t.is(body, expectedBody)
        onRequestEnd()
      }))
      request.end()
    })

    request.on('error', t.end)
  }

  session.on('socketError', t.end)
  session.on('error', t.end)
})

test.cb('HTTP/2 Misdirected Request on unknown hostname', (t) => {
  const session = connect(
    'https://0.0.0.0:8443',
    {rejectUnauthorized: false}
  )

  const request = session.request({':path': '/'})
  request.once('response', (headers) => {
    t.is(headers[':status'], 421)

    request.pipe(concat((data) => {
      const expectedBody = 'Misdirected Request'
      const body = new StringDecoder().end(data)
      t.is(body, expectedBody)
      t.end()
    }))
    request.end()
  })

  request.on('error', t.end)

  session.on('socketError', t.end)
  session.on('error', t.end)
})

test.cb('HTTP/2 200 fallback', (t) => {
  const session = connect(
    'https://127.0.0.1:8443',
    {rejectUnauthorized: false}
  )

  const request = session.request({':path': '/foobar'})
  request.once('response', (headers) => {
    t.is(headers[':status'], 200)

    request.pipe(concat((data) => {
      const expectedBody = 'Stay awhile and listen.'
      const body = new StringDecoder().end(data)
      t.is(body, expectedBody)
      t.end()
    }))
    request.end()
  })

  request.on('error', t.end)

  session.on('socketError', t.end)
  session.on('error', t.end)
})

test.cb('HTTP/2 404 fallback', (t) => {
  const session = connect(
    'https://localhost:8443',
    {rejectUnauthorized: false}
  )

  const request = session.request({':path': '/foobar'})
  request.once('response', (headers) => {
    t.is(headers[':status'], 404)

    request.pipe(concat((data) => {
      const expectedBody = 'Ah ah ah! You didn\'t say the magic word!'
      const body = new StringDecoder().end(data)
      t.is(body, expectedBody)
      t.end()
    }))
    request.end()
  })

  request.on('error', t.end)

  session.on('socketError', t.end)
  session.on('error', t.end)
})

test.cb('HTTP/2 HSTS header', (t) => {
  const session = connect(
    'https://localhost:8443',
    {rejectUnauthorized: false}
  )

  const request = session.request({':path': '/'})
  request.once('response', (headers) => {
    const expectedHsts = `max-age=${60 * 24 * 3600}`
    t.is(headers['strict-transport-security'], expectedHsts)
    request.end()
    t.end()
  })

  request.on('error', t.end)

  session.on('socketError', t.end)
  session.on('error', t.end)
})
