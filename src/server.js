const {join, resolve} = require('path')
const {promisify} = require('util')
const {readFile, readFileSync} = require('fs')
const {app} = require('./app')
const ocsp = require('ocsp')
const {createSecureServer} = require('http2')
const {createSecureContext} = require('tls')
const isDomainName = require('is-domain-name')
const isIp = require('is-ip')

async function read (filepath) {
  const resolved = resolve(process.cwd(), filepath)
  return promisify(readFile)(resolved)
}

function readSync (filepath) {
  const resolved = resolve(process.cwd(), filepath)
  return readFileSync(resolved)
}

module.exports.server = (options, files) => {
  const requestListener = app(options, files)

  const ecdhCurve = 'P-384:P-256'
  const fallbackKey = readSync(options.https.key)
  const fallbackCert = readSync(options.https.cert)
  const fallbackCa = options.https.ca.map(readSync)

  function fallbackSecureContext () {
    return createSecureContext({
      ecdhCurve,
      key: fallbackKey,
      cert: fallbackCert,
      ca: fallbackCa
    })
  }

  const acmeCache = new Map()
  const serverOptions = {
    allowHTTP1: true,
    ecdhCurve,
    key: fallbackKey,
    cert: fallbackCert,
    ca: fallbackCa,
    SNICallback: async (servername, callback) => {
      if (acmeCache.has(servername)) {
        const {key, cert} = acmeCache.get(servername)
        const contextOptions = {ecdhCurve, key, cert}
        return callback(null, createSecureContext(contextOptions))
      }

      if (!(isDomainName(servername) || isIp(servername))) {
        return callback(null, fallbackSecureContext())
      }

      try {
        const [key, cert] = await Promise.all([
          read(join(options.acme.store, servername, 'key.pem')),
          read(join(options.acme.store, servername, 'cert.pem'))
        ])
        const contextOptions = {ecdhCurve, key, cert}
        acmeCache.set(servername, {key, cert})
        const context = createSecureContext(contextOptions)
        return callback(null, context)
      } catch (error) {
        if (error.code !== 'ENOENT') {
          console.error(error)
        }
        return callback(null, fallbackSecureContext())
      }
    }
  }

  const server = createSecureServer(serverOptions, requestListener)

  const ocspCache = new ocsp.Cache()
  server.on('OCSPRequest', (certificate, issuer, callback) => {
    if (!issuer) return callback()
    ocsp.getOCSPURI(certificate, (error, uri) => {
      if (error) return callback(error)
      if (uri === null) return callback()
      const request = ocsp.request.generate(certificate, issuer)
      ocspCache.probe(request.id, (error, {response}) => {
        if (error) return callback(error)
        if (response) return callback(null, response)
        const options = {
          url: uri,
          ocsp: request.data
        }
        ocspCache.request(request.id, options, callback)
      })
    })
  })

  return server
}
