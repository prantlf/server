const debug = require('debug')
const cluster = require('cluster')
const physicalCpuCount = require('physical-cpu-count')
const eventToPromise = require('event-to-promise')
const {redirect} = require('./redirect')
const {lock, unlock} = require('./helpers/pid')
const {normaliseManifest} = require('./helpers/normaliseManifest')
// const {watch} = require('chokidar')
// const debounce = require('debounce-collect')
const recursiveReaddir = require('recursive-readdir')
const {Configuration} = require('./configuration/Configuration')
const {Parser} = require('expr-eval')

function evaluateToNumber (expression) {
  if (typeof expression === 'number') return expression
  return Parser.evaluate(
    expression,
    {max_physical_cpu_cores: physicalCpuCount}
  )
}

module.exports.Master = class Master {
  constructor (options, argv) {
    this.workers = []
    this.options = options
    this.argv = argv
    this.log = debug(process.title)
  }

  stop () {
    console.log('')
    process.exit()
  }

  async start () {
    const options = this.options

    lock()
    process.on('exit', unlock)
    process.on('SIGINT', this.stop.bind(this))
    process.on('SIGHUP', this.reload.bind(this))

    await this._load()

    if (options.http.redirect === true) {
      this.redirector = await redirect({
        http: options.http,
        acme: options.acme
      })
    }

    this.log('Server started')

    // const broadcastExpireToWorkers = debounce(function (paths) {
    //   for (const path of new Set(paths)) {
    //     for (const worker of workers) {
    //       worker.send({expire: path})
    //     }
    //   }
    // }, 50)

    // watch(options.root, {})
    //   .on('change', broadcastExpireToWorkers)
    //   .on('unlink', broadcastExpireToWorkers)
  }

  async _load () {
    const files = {}
    for (const host of this.options.hosts) {
      const {root, domain, manifest} = host

      // Scan files so workers can build a cached lookup index.
      // See also: hostOptions middleware
      files[domain] = {root, index: await recursiveReaddir(root)}

      host.manifest = normaliseManifest(manifest)
    }

    const workers = []
    const maxWorkerCount = evaluateToNumber(this.options.workers.count)
    if (maxWorkerCount < 1) throw new Error('Worker count must be at least 1')
    cluster.setupMaster({exec: require.resolve('./worker.js')})
    while (workers.length < maxWorkerCount) workers.push(cluster.fork())
    await Promise.all(workers.map((worker) => eventToPromise(worker, 'online')))
    workers.forEach((worker) => worker.send({options: this.options, files}))
    await Promise.all(workers.map((worker) => eventToPromise(worker, 'listening')))
    this.workers = workers

    for (const {root, domain} of this.options.hosts) {
      let authority = domain
      if (this.options.https.port !== 443) {
        authority += ':' + this.options.https.port
      }
      this.log(`Serving ${root} on port https://${authority}`)
    }
  }

  async reload () {
    this.log('Reloading configuration')
    try {
      const configuration = new Configuration(this.argv)
      this.options = await configuration.load()
    } catch (error) {
      this.log(error)
      return
    }

    const retirees = this.workers
    await this._load()
    for (const retiree of retirees) {
      retiree.kill()
    }
  }
}
