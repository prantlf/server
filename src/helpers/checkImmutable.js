const emojiRegex = require('emoji-regex')

const isEmojiRevved = new RegExp(
  '.+' + // Base filename
  '[-_.]' + // Separator
  `(?:${emojiRegex().source})+` + // Emoji ranges
  '(?:\\.\\w+)+$' // File extension(s)
)

const isHexRevved = new RegExp(
  '.+' + // Base filename
  '[-_.]' + // Separator
  '[0-9a-fA-F]+' + // Hexadecimal hash
  '(?:\\.\\w+)+$'// File extension(s)
)

const regexes = Object.create(null)

module.exports.checkImmutable = (filepath, patterns) => {
  for (const pattern of patterns) {
    if (!(pattern in regexes)) {
      regexes[pattern] = pattern === 'emoji' ? isEmojiRevved
        : pattern === 'hex' ? isHexRevved
        : new RegExp(pattern)
    }
    if (regexes[pattern].test(filepath)) {
      return true
    }
  }
  return false
}
