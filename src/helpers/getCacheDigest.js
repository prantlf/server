function parseHeader (payload) {
  const digests = payload.split(', ')
  for (const digest of digests) {
    const [value, ...flags] = digest.split('; ')
    if (flags.includes('complete')) {
      return value
    }
  }
}

module.exports.getCacheDigest = (request, cookies) => {
  const cookie = cookies.get('cache-digest')
  if (cookie) {
    return cookie
  }

  const header = request.headers['cache-digest']
  if (header) {
    return parseHeader(header)
  }
}
